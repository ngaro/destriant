FROM httpd:alpine
RUN apk add git make sassc
WORKDIR /root
RUN git clone https://gitlab.com/artefact2/destriant
WORKDIR /root/destriant 
RUN make
RUN mv /root/destriant/public/* /usr/local/apache2/htdocs
